#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"

m, n = [int(s) for s in input().split()]
a = [[int(j) for j in input().split()] for i in range(m)]
max_valor, max_i, max_j = a[0][0], 0, 0
for i in range(m):
    for j in range(n):
        if a[i][j] > max_valor:
            max_valor, max_i, max_j = a[i][j], i, j
print(max_i, max_j)
