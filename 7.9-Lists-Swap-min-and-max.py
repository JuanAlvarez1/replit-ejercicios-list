#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"

a = [int(s) for s in input().split()]
max, min = a.index(max(a)), a.index(min(a))
a[max], a[min] = a[min], a[max]

print(a)
