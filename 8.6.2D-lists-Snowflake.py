#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"
num = int(input())
a = [["."]*num for i in range(num)]
for y in range(num):
    for x in range(num):
        if x == y:
            a[y][x] = '*'
        elif x == (num-1)/2:
            a[y][x] = '*'
        elif x + y == 6:
            a[y][x] = '*'
        elif y == (num-1)/2:
            a[y][x] = '*'
for var in a:
    print(*var)
