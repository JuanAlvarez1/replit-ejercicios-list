#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"

a = input()
lista1 = list(map(int, a.split()))
var1 = lista1[0]
var2 = lista1[1]
lista2 = []
n = 1

while n <= var2:
    a = input()
    mylista = list(map(int, a.split()))
    lista2.append(mylista[0])
    lista2.append(mylista[1])
    n += 1

lista3 = list(range(1, var1 + 1))
x = 0
for x in range(0, len(lista2) - 1, 2):
    if lista2[x + 1] > lista2[x]:
        dist = list(range(lista2[x], lista2[x + 1] + 1))
    elif lista2[x + 1] < lista2[x]:
        dist = list(range(lista2[x + 1], lista2[x] + 1))
    else:
        dist = [lista2[x]]

    for d in dist:
        if d in lista3:
            lista3[lista3.index(d)] = '.'
for e in lista3:
    if e == '.':
        continue
    else:
        lista3[lista3.index(e)] = 'I'
print(str(lista3).replace('[', '').replace(']', '').replace("'", "").replace(",", ""))
