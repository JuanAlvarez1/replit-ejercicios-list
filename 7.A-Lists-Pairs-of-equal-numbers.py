#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"

x = input()
a = list(map(int, x.split()))
par = 0
for x in a:
    for n in range(0, len(a)):
        if a[n] == x and n != a.index(x):
            par += 1
par /= 2
print(int(par))
