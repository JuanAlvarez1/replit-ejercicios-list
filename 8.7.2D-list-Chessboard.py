#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"

n, m = [int(s) for s in input().split()]
a = [["."]*m for i in range(n)]
for i in range (n):
  for j in range (m):
    if j%2 != 0 and i%2 == 0:
      a[i][j] = '*'
    elif j%2 == 0 and j%2 != 0:
      a[i][j] = '*'
for k in a:
    print(* k)
